extern crate chrono;
extern crate time;

use std::fmt;
use std::net::UdpSocket;
use std::str;
use std::string::String;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;
use time::Duration;

use chrono::prelude::*;

fn query_ntp(addr: &str) -> NTPData {
    let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
    let mut buf = NTPData::new(addr);

    socket.send_to(&buf.pkt, addr).unwrap();
    socket.recv_from(&mut buf.pkt).unwrap();
    buf.got = Utc::now();
    return buf;
}

fn query_ntp_best_burst(addr: &str) -> NTPData {
    let mut best = query_ntp(addr);
    for _ in 0..7 {
        let got = query_ntp(addr);
        if got.delay() < best.delay() {
            best = got;
        }
    }
    return best;
}

fn make_timestamp_64(date_time: &DateTime<Utc>) -> [u8; 8] {
    let dur = date_time
        .signed_duration_since(Utc.ymd(1900, 1, 1).and_hms(0, 0, 0))
        .to_std()
        .unwrap();
    let x = (dur.as_secs() << 32) + dur.subsec_nanos() as u64;
    let b1: u8 = ((x >> 56) & 0xff) as u8;
    let b2: u8 = ((x >> 48) & 0xff) as u8;
    let b3: u8 = ((x >> 40) & 0xff) as u8;
    let b4: u8 = ((x >> 32) & 0xff) as u8;
    let b5: u8 = ((x >> 24) & 0xff) as u8;
    let b6: u8 = ((x >> 16) & 0xff) as u8;
    let b7: u8 = ((x >> 8) & 0xff) as u8;
    let b8: u8 = (x & 0xff) as u8;
    return [b1, b2, b3, b4, b5, b6, b7, b8];
}

struct NTPData {
    addr: String,
    pkt: [u8; 48],
    got: DateTime<Utc>,
}

impl NTPData {
    fn new(addr: &str) -> NTPData {
        let mut buf = NTPData {
            addr: addr.to_string(),
            pkt: [0; 48],
            got: Utc::now(),
        };
        buf.pkt[0] = 27;
        let now = make_timestamp_64(&Utc::now());
        for (i, byte) in now.into_iter().enumerate() {
            // populate the transmit timestamp field
            buf.pkt[40 + i] = *byte;
        }
        return buf;
    }

    fn leap_indicator(&self) -> u8 {
        (self.pkt[0] & 0b11000000) >> 6
    }
    fn version_number(&self) -> u8 {
        (self.pkt[0] & 0b00111000) >> 3
    }
    fn ntp_packet_mode(&self) -> u8 {
        self.pkt[0] & 0b00000111
    }
    fn stratum(&self) -> u8 {
        self.pkt[1]
    }
    fn poll(&self) -> u8 {
        self.pkt[2]
    }
    fn precision(&self) -> i8 {
        self.pkt[3] as i8
    }

    fn fixed_i32_to_float(&self, byte_slice: &[u8]) -> f32 {
        let mut result: i32 = 0;
        for (i, byte) in byte_slice.iter().rev().enumerate() {
            result += ((*byte as u32) << (i * 8)) as i32;
        }
        (result as f32) / 65536.0
    }

    fn root_delay(&self) -> f32 {
        self.fixed_i32_to_float(&self.pkt[4..8])
    }

    fn root_dispersion(&self) -> f32 {
        self.fixed_i32_to_float(&self.pkt[8..12])
    }

    fn ref_id(&self) -> String {
        if self.stratum() <= 1 {
            // 4 byte string identifier if stratum 1 or below.
            return str::from_utf8(&self.pkt[12..16]).unwrap().to_string();
        }
        // Otherwise the IP address of the parent NTP server.
        self.pkt[12..16]
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join(".")
    }

    fn fixed_u64_to_float(&self, byte_slice: &[u8]) -> f64 {
        let mut result: u64 = 0;
        for (i, byte) in byte_slice.iter().rev().enumerate() {
            result += (*byte as u64) << (i * 8);
        }
        (result as f64) / 4_294_967_296.0
    }

    fn ref_timestamp(&self) -> f64 {
        self.fixed_u64_to_float(&self.pkt[16..24])
    }
    fn ref_timestamp_d(&self) -> DateTime<Utc> {
        let nanos = (self.ref_timestamp().fract() * 1e9) as i64;
        Utc.ymd(1900, 1, 1).and_hms(0, 0, 0) + Duration::seconds(self.ref_timestamp() as i64)
            + Duration::nanoseconds(nanos)
    }

    fn org_timestamp(&self) -> f64 {
        self.fixed_u64_to_float(&self.pkt[24..32])
    }
    fn org_timestamp_d(&self) -> DateTime<Utc> {
        let nanos = (self.org_timestamp().fract() * 1e9) as i64;
        Utc.ymd(1900, 1, 1).and_hms(0, 0, 0) + Duration::seconds(self.org_timestamp() as i64)
            + Duration::nanoseconds(nanos)
    }

    fn rcv_timestamp(&self) -> f64 {
        self.fixed_u64_to_float(&self.pkt[32..40])
    }
    fn rcv_timestamp_d(&self) -> DateTime<Utc> {
        let nanos = (self.rcv_timestamp().fract() * 1e9) as i64;
        Utc.ymd(1900, 1, 1).and_hms(0, 0, 0) + Duration::seconds(self.rcv_timestamp() as i64)
            + Duration::nanoseconds(nanos)
    }

    fn txm_timestamp(&self) -> f64 {
        self.fixed_u64_to_float(&self.pkt[40..48])
    }
    fn txm_timestamp_d(&self) -> DateTime<Utc> {
        let nanos = (self.txm_timestamp().fract() * 1e9) as i64;
        Utc.ymd(1900, 1, 1).and_hms(0, 0, 0) + Duration::seconds(self.txm_timestamp() as i64)
            + Duration::nanoseconds(nanos)
    }

    fn offset(&self) -> Duration {
        ((self.rcv_timestamp_d()
            .signed_duration_since(self.org_timestamp_d()))
            + (self.txm_timestamp_d().signed_duration_since(self.got))) / 2
    }

    fn delay(&self) -> Duration {
        (self.got.signed_duration_since(self.org_timestamp_d()))
            - (self.txm_timestamp_d()
                .signed_duration_since(self.rcv_timestamp_d()))
    }
}

impl fmt::Display for NTPData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "## {}:\n", self.addr)?;
        write!(f, "Leap Indicator:\n")?;
        let li = self.leap_indicator();
        write!(f, " {} - ", li)?;
        match li {
            0 => write!(f, "No leap second adjustment"),
            1 => write!(f, "Last minute of the day has 61 seconds"),
            2 => write!(f, "Last minute of the day has 59 seconds"),
            3 => write!(f, "Clock is unsynchronized"),
            _ => write!(f, "Invalid"),
        }?;
        write!(f, "\n")?;

        write!(f, "Version Number:\n")?;
        write!(f, " {}\n", self.version_number())?;

        write!(f, "NTP packet mode:\n")?;
        let pm = self.ntp_packet_mode();
        write!(f, " {} - ", pm)?;
        match pm {
            0 => write!(f, "Reserved"),
            1 => write!(f, "Symmetric active"),
            2 => write!(f, "Symmetric passive"),
            3 => write!(f, "Client"),
            4 => write!(f, "Server"),
            5 => write!(f, "Broadcast"),
            6 => write!(f, "NTP control message"),
            7 => write!(f, "Reserved for private use"),
            _ => write!(f, "Invalid"),
        }?;
        write!(f, "\n")?;

        write!(f, "Stratum:\n")?;
        let stratum = self.stratum();
        write!(f, " {} - ", stratum)?;
        match stratum {
            0 => write!(f, "Unspecified/invalid"),
            1 => write!(f, "Primary server"),
            2...15 => write!(f, "Secondary server"),
            16 => write!(f, "Unsynchronized"),
            17...255 => write!(f, "Reserved"),
            _ => write!(f, "Invalid"),
        }?;
        write!(f, "\n")?;

        write!(f, "Poll Interval:\n")?;
        let poll = self.poll();
        write!(f, " {}", poll)?;
        write!(f, "\n")?;

        write!(f, "Clock Precision:\n")?;
        let precision = self.precision();
        write!(
            f,
            "{} ({} ms)",
            precision,
            ((precision as f64).exp2() * 1000.0)
        )?;
        write!(f, "\n")?;

        write!(f, "Root Delay:\n")?;
        write!(f, " {} s", self.root_delay())?;
        write!(f, "\n")?;

        write!(f, "Root Dispersion:\n")?;
        write!(f, " {} s", self.root_dispersion())?;
        write!(f, "\n")?;

        write!(f, "Reference Identifier:\n")?;
        write!(f, " {}", self.ref_id())?;
        write!(f, "\n")?;

        write!(f, "Reference Timestamp:\n")?;
        write!(f, " {}", self.ref_timestamp_d())?;
        write!(f, "\n")?;

        write!(f, "Origin Timestamp:\n")?;
        write!(f, " {}", self.org_timestamp_d())?;
        write!(f, "\n")?;

        write!(f, "Receive Timestamp:\n")?;
        write!(f, " {}", self.rcv_timestamp_d())?;
        write!(f, "\n")?;

        write!(f, "Transmit Timestamp:\n")?;
        write!(f, " {}", self.txm_timestamp_d())?;
        write!(f, "\n")?;

        write!(f, "Got Timestamp:\n")?;
        write!(f, " {}", self.got)?;
        write!(f, "\n")?;

        write!(f, "Offset:\n")?;
        write!(f, " {}", self.offset())?;
        write!(f, "\n")?;

        write!(f, "Delay:\n")?;
        write!(f, " {}", self.delay())?;
        write!(f, "\n")?;

        Ok(())
    }
}

impl fmt::Debug for NTPData {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        self.pkt[..].fmt(formatter)
    }
}

fn main() {
    let addrs = [
        "50.73.112.122:123",
        "time1.google.com:123",
        "time2.google.com:123",
        "time3.google.com:123",
        "time4.google.com:123",
        "0.freebsd.pool.ntp.org:123",
    ];

    let (tx, rx): (Sender<NTPData>, Receiver<NTPData>) = mpsc::channel();

    for addr in addrs.iter() {
        let thread_tx = tx.clone();
        let thread_addr = addr.clone();
        thread::spawn(move || {
            let query_result = query_ntp_best_burst(thread_addr);
            thread_tx.send(query_result).unwrap();
        });
    }
    for _ in 0..addrs.len() {
        let got = rx.recv().unwrap();
        println!{"\n{}", got}
    }
}
